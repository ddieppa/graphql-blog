import { Component, OnInit } from '@angular/core';
import { ExternalScriptLoader } from 'src/app/utilities/global-functions';

@Component({
  selector: 'app-read-article',
  templateUrl: './read-article.component.html',
  styleUrls: ['./read-article.component.css']
})
export class ReadArticleComponent implements OnInit {

  constructor() { 
    ExternalScriptLoader.loadScript();
  }

  ngOnInit(): void {
  }

}
