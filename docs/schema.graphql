schema {
  query: AppQuery
  mutation: AppMutation
}

# The cost directives is used to express the complexity of a field.
directive @cost(
  # Defines the complexity of the field.
  complexity: Int! = 1
  # Defines field arguments that act as complexity multipliers.
  multipliers: [MultiplierPath!]
) on FIELD_DEFINITION
directive @authorize(
  # The name of the authorization policy that determines access to the annotated resource.
  policy: String
  # Roles that are allowed to access to the annotated resource.
  roles: [String!]
) on OBJECT | FIELD_DEFINITION
type AppMutation {
  login(input: LoginInput): AuthToken
  postComment(input: CommentInput): [Comment]
  signup(input: SignupInput): AuthToken
  writeArticle(input: ArticleInput): Article
}

type AppQuery {
  article(articleId: Int!): Article
  articles: [Article]
  categories: [Category]
  tags: [Tag]
}

type Article {
  category: Category!
  comments: [Comment]
  id: Int!
  publishedAt: DateTime!
  tags: [ArticleTag]
  text: String!
  title: String!
}

input ArticleInput {
  categoryId: Int!
  tagIds: [Int!]
  text: String!
  title: String!
}

type ArticleTag {
  tag: Tag
}

type AuthToken {
  accessToken: String
  expiresIn: DateTime!
  tokenType: String
  username: String
}

type Category {
  articleCount: Int!
  description: String
  id: Int!
  name: String!
}

type Comment {
  id: Int!
  postedAt: DateTime!
  text: String!
}

input CommentInput {
  articleId: Int!
  commentText: String!
}

# The `DateTime` scalar represents
 an ISO-8601 compliant date time type.
scalar DateTime

input LoginInput {
  email: String!
  password: String!
}

# The multiplier path scalar represents
 a valid GraphQL multiplier path string.
scalar MultiplierPath

input SignupInput {
  confirmPassword: String!
  email: String!
  name: String!
  password: String!
}

type Tag {
  id: Int!
  name: String!
}

