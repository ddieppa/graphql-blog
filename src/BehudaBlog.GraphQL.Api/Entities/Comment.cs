﻿using System;
using HotChocolate;

namespace BehudaBlog.GraphQL.Api.Entities
{
    public class Comment
    {
        [GraphQLNonNullType]
        public int Id { get; set; }
        [GraphQLNonNullType]
        public string Text { get; set; }
        [GraphQLNonNullType]
        public DateTime PostedAt { get; set; }
        [GraphQLIgnore]
        public int ArticleId { get; set; }
        [GraphQLIgnore]
        public virtual Article Article { get; set; }
        [GraphQLIgnore]
        public string CommentedById { get; set; }
        [GraphQLIgnore]
        public virtual ApplicationUser CommentedBy { get; set; }

        public User Commentator => new User()
        {
            UserId = this.CommentedBy.Id,
            Name = CommentedBy.Name
        };
    }
}
