﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BehudaBlog.GraphQL.Api.Data;
using BehudaBlog.GraphQL.Api.Entities;
using BehudaBlog.GraphQL.Api.Helpers;
using BehudaBlog.GraphQL.Api.ViewModels;
using HotChocolate;
using HotChocolate.AspNetCore.Authorization;
using HotChocolate.Execution;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BehudaBlog.GraphQL.Api.GraphQL
{
    public class AppMutation
    {
        public async Task<AuthToken> SignupAsync(
            SignupInput input,
            [Service]UserManager<ApplicationUser> userManager)
        {
            try
            {
                var user = new ApplicationUser()
                {
                    Name = input.Name,
                    UserName = input.Email,
                    Email = input.Email,
                };

                var result = await userManager.CreateAsync(user, input.Password);
                if (!result.Succeeded)
                    throw new QueryException(string.Join(',', result.Errors.Select(e => e.Description)));

                var token = AppAuthenticationHelper.GenerateAuthToken(user);
                return token;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new QueryException(e.Message);
            }
        }

        public async Task<AuthToken> LoginAsync(
            LoginInput input,
            [Service] UserManager<ApplicationUser> userManager)
        {
            try
            {
                var user = await userManager.Users.FirstOrDefaultAsync(u => u.Email == input.Email);
                if (user == null || !await userManager.CheckPasswordAsync(user, input.Password))
                    throw new QueryException("Email or password invalid.");

                var token = AppAuthenticationHelper.GenerateAuthToken(user);
                return token;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [Authorize]
        public async Task<Article> WriteArticle(
            ArticleInput input,
            [GlobalState]string currentUserId,
            [Service] ApplicationDbContext dbContext)
        {
            try
            {
                var article = new Article()
                {
                    Title = input.Title,
                    CategoryId = input.CategoryId,
                    Text = input.Text,
                    PublishedAt = DateTime.Now,
                    Tags = new List<ArticleTag>(),
                    WrittenById = currentUserId
                };
                foreach (var inputTagId in input.TagIds)
                {
                    article.Tags.Add(new ArticleTag() {TagId = inputTagId});
                }

                await dbContext.Articles.AddAsync(article);
                if (await dbContext.SaveChangesAsync() > 0)
                {
                    return article;
                }

                throw new QueryException("Article couldn't be saved.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [Authorize]
        public async Task<List<Comment>> PostComment(
            CommentInput input,
            [GlobalState]string currentUserId,
            [Service]ApplicationDbContext dbContext)
        {
            try
            {
                if(await dbContext.Articles.FirstOrDefaultAsync(a => a.Id == input.ArticleId) == null)
                    throw new QueryException("Article not found.");

                var comment = new Comment()
                {
                    ArticleId = input.ArticleId,
                    Text = input.CommentText,
                    PostedAt = DateTime.Now,
                    CommentedById = currentUserId
                };

                await dbContext.Comments.AddAsync(comment);
                if (await dbContext.SaveChangesAsync() <= 0) throw new QueryException("Couldn't comment.'");

                var comments = await dbContext.Comments.Where(c => c.ArticleId == input.ArticleId).ToListAsync();
                return comments;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
