@echo off
color a
git pull origin master

start cmd /K run-angular

dotnet ef database update --project src/BehudaBlog.GraphQL.Api

dotnet build
dotnet run --project src/BehudaBlog.GraphQL.Api

PAUSE