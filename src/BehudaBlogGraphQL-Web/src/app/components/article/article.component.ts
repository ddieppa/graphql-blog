import { Component, OnInit, Input } from '@angular/core';
import { Article } from 'src/app/models/article';
import { ExternalScriptLoader } from 'src/app/utilities/global-functions';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  @Input() data: Article; 

  constructor() { }

  ngOnInit(): void {
    ExternalScriptLoader.loadScript();
    console.log(this.data);
  }

}
