import { Component, OnInit } from '@angular/core';
import { ExternalScriptLoader } from 'src/app/utilities/global-functions';
import { ArticleEdge } from 'src/app/models/article';
import { PageInfo } from "src/app/models/page-info";
import gql from 'graphql-tag';
import { Apollo } from 'apollo-angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private total = 0;
  private limit = 5;
  private lastCursor = "";

  public latestArticles: ArticleEdge[] = [];
  public pageInfo: PageInfo;
  public pageNumber = 1;
  public pages: number[] = [];

  constructor(private apollo: Apollo,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
    ExternalScriptLoader.loadScript();

    let param = this.activatedRoute.snapshot.paramMap.get("page");
    this.pageNumber = param == "" ? 1 : +param;
  }

  ngOnInit() {
    if (this.pageNumber != 1) this.getLastCursor(this.pageNumber);
    else this.getPageData();
  }

  changePage(pageNumber: number){
    this.pageNumber = pageNumber;
    this.router.navigate(["/articles", this.pageNumber]);
    this.getLastCursor(this.pageNumber);

    window.scroll(0,0);
  }

  private getPageData() {
    let queryNode = gql`
    query getArticles($limit: PaginationAmount!, $lastCursor: String){
  articles (
    first: $limit
    after: $lastCursor
    order_by: {
      publishedAt: DESC
    }
  ){
    articleEdges: edges {
      article: node {
        id
        title
        publishedAt
        category {
          id
          name
        }
        commentsCount
        text
      }
      cursor
    }
    pageInfo {
      hasNextPage
      hasPreviousPage
      startCursor
      endCursor
    }
    totalCount
  }
}`;

    let variables = {
      limit: this.limit,
      lastCursor: this.lastCursor,
    }
    console.log(variables);
    this.apollo.query<any>({
      query: queryNode,
      variables: variables
    }).subscribe(result => {
      console.log(result);
      this.latestArticles = result.data && result.data.articles.articleEdges;
      this.pageInfo = result.data && result.data.articles.pageInfo;
      this.total = result.data && result.data.articles.totalCount;

      //Generating pages
      this.pages = [];
      if (this.pageNumber < 3) {
        for (let i = 1; i <= Math.min(Math.ceil((this.total / this.limit)), 5); i++) {
          this.pages.push(i);
        }
      } else {
        for (let i = this.pageNumber - 2; i <= Math.min((this.total / this.limit), this.pageNumber + 2); i++) {
          this.pages.push(i);
        }
      }
      //console.log(this.pages)
    })
  }

  private getLastCursor(pageNum: number) {
    let variable = {
      first: (pageNum - 1) * this.limit
    }

    let queryNode = gql`
    query lastCursor($first: PaginationAmount!){
  articles(
    first: $first,
    order_by: {
      publishedAt: DESC
    }
  ){
    pageInfo{
      endCursor
    }
  }
}`;

    this.apollo.query<any>({
      query: queryNode,
      variables: variable
    }).subscribe(result => {
      console.log(result);
      this.lastCursor = result.data && result.data.articles.pageInfo.endCursor;
      this.getPageData();
    });
  }
}
