﻿using System.Collections.Generic;
using HotChocolate;

namespace BehudaBlog.GraphQL.Api.ViewModels
{
    public class ArticleInput
    {
        [GraphQLNonNullType]
        public string Title { get; set; }
        [GraphQLNonNullType]
        public int CategoryId { get; set; }
        [GraphQLNonNullType]
        public string Text { get; set; }
        public List<int> TagIds { get; set; }
    }
}
