﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BehudaBlog.GraphQL.Api.Migrations
{
    public partial class UserRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CommentedById",
                table: "Comments",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WrittenById",
                table: "Articles",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Comments_CommentedById",
                table: "Comments",
                column: "CommentedById");

            migrationBuilder.CreateIndex(
                name: "IX_Articles_WrittenById",
                table: "Articles",
                column: "WrittenById");

            migrationBuilder.AddForeignKey(
                name: "FK_Articles_AspNetUsers_WrittenById",
                table: "Articles",
                column: "WrittenById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_AspNetUsers_CommentedById",
                table: "Comments",
                column: "CommentedById",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articles_AspNetUsers_WrittenById",
                table: "Articles");

            migrationBuilder.DropForeignKey(
                name: "FK_Comments_AspNetUsers_CommentedById",
                table: "Comments");

            migrationBuilder.DropIndex(
                name: "IX_Comments_CommentedById",
                table: "Comments");

            migrationBuilder.DropIndex(
                name: "IX_Articles_WrittenById",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "CommentedById",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "WrittenById",
                table: "Articles");
        }
    }
}
