import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { Tag } from 'src/app/models/tag';

@Component({
  selector: 'app-tag-cloud',
  templateUrl: './tag-cloud.component.html',
  styleUrls: ['./tag-cloud.component.css']
})
export class TagCloudComponent implements OnInit {

  isLoading:boolean = true;
  tags: Tag[];

  constructor(
    private apollo: Apollo
  ) { }

  ngOnInit(): void {
    this.loadTags();
  }

  private loadTags() {
    let queryNode = gql`
    {
      tags{
        id
        name
      }
    }`;

    this.apollo.query<any>({
      query: queryNode
    }).subscribe(result => {
      console.log(result);
      this.isLoading = result.loading;
      this.tags = result.data && result.data.tags;
    });
  }

}
