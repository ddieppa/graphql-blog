﻿using System.Collections.Generic;
using BehudaBlog.GraphQL.Api.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BehudaBlog.GraphQL.Api.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) {}

        public DbSet<Article> Articles { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Article & Tag Relation...
            modelBuilder.Entity<ArticleTag>().HasKey(at => new {at.ArticleId, at.TagId});
            modelBuilder.Entity<Article>()
                .HasMany<ArticleTag>(a => a.Tags)
                .WithOne(at => at.Article)
                .HasForeignKey(at => at.ArticleId);
            modelBuilder.Entity<Tag>()
                .HasMany<ArticleTag>(t => t.Articles)
                .WithOne(at => at.Tag)
                .HasForeignKey(at => at.TagId);

            //Category seed data...
            modelBuilder.Entity<Category>().HasData(new List<Category>()
            {
                new Category(){Id = 1, Name = "Technology"},
                new Category(){Id = 2, Name = "Lifestyle"},
                new Category(){Id = 3, Name = "Travel"},
                new Category(){Id = 4, Name = "Cooking"},
            });

            //Tags seed data...
            modelBuilder.Entity<Tag>().HasData(new List<Tag>()
            {
                new Tag(){Id = 1, Name = "Tech"},
                new Tag(){Id = 2, Name = "ASP.NET"},
                new Tag(){Id = 3, Name = "ASP.NET Core"},
                new Tag(){Id = 4, Name = "Lifestyle"},
                new Tag(){Id = 5, Name = "Cooking"},
                new Tag(){Id = 6, Name = "Pasta"},
                new Tag(){Id = 7, Name = "Biriyani"},
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
