﻿using System;

namespace BehudaBlog.GraphQL.Api.ViewModels
{
    public class AuthToken
    {
        public string AccessToken { get; set; }
        public string TokenType { get; set; }
        public DateTime ExpiresIn { get; set; }
        public string Username { get; set; }

        public AuthToken() { }

        public AuthToken(string accessToken, DateTime expiresIn, string username)
        {
            AccessToken = accessToken;
            TokenType = "Bearer";
            ExpiresIn = expiresIn;
            Username = username;
        }
    }
}
