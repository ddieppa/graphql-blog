import { Category } from './category';

export interface Article {
    id: number;
    title: string;
    publishedAt: Date;
    category: Category;
    commentsCount: number;
    text: string;
}

export interface ArticleEdge {
    article: Article;
    cursor: string;
}